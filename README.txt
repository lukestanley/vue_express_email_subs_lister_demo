IMPLEMENTED:
    Basic backend with POST adding and GET listing.
    Basic frontend with adding and listing
    Simple auth system
    PHP CSV export
    Map button and modal
    Browser and server side email validation
    Deletion
    CDN hosted files

TODO:
    Backend:
        Pagination

    Client side:
        Enter to submit
