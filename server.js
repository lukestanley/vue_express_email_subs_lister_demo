var express = require('express');
var server = express();
var sqlite3 = require('sqlite3').verbose();
var validator = require("email-validator");

server.post('/session', function(req, res){
	password = req.query['password'];
	
	if (password === "password"){
		res.status(201);
		res.json({'session': 'magic_valid_session'});
	}
	else {
		res.status(401);
		res.end();
	}
	
});

server.use('/subscribers', function (req, res, next) {
  console.log('Request URL:', req.originalUrl)
  has_auth = false;
  if (typeof req.headers['session'] != 'undefined'){
      if (req.headers['session']==='magic_valid_session'){
		  has_auth = true;
	  }
  }
  if (has_auth === true){
	next() 
  } else {
	  res.status(401);
	  res.end();
  }
}, function (req, res, next) {
  console.log('Request Type:', req.method)
  next()
})

server.use(express.static('client'));

var db = new sqlite3.Database('database.sqlite');
//TODO: use ORM e.g: knex
//TODO: use Postgres
db.serialize(function() {
    db.run("CREATE TABLE IF NOT EXISTS `subscribers` ( \
				`email`	TEXT, \
				`name`	TEXT, \
				`postcode`	TEXT, \
				PRIMARY KEY(email) \
			);");
});

server.get('/subscribers', function(req, res){
	//TODO: implement pagination
    db.all("SELECT email, name, postcode FROM subscribers", function(err, results){
		res.json(results);
    });
});

server.post('/subscribers', function(req, res){
	email = req.query['email'];
	name = req.query['name'];
	postcode = req.query['postcode'];
	
	//TODO: validate we have all the paramaters
	
	if (validator.validate(email)) {
		
		db.run("INSERT INTO subscribers VALUES (?,?,?)", [email, name, postcode], function(err, row){
			if (err){
				console.err(err);
				res.status(500);
			}
			else {
				res.status(202);
			}
			res.end();
		});
    
	} else {
		console.err(err);
		res.status(400);
	}
});

server.delete('/subscribers', function(req, res){
	email = req.query['email'];
	
	db.run("DELETE FROM subscribers WHERE email = ?", [email], function(err, row){
		if (err){
			console.err(err);
			res.status(500);
		}
		else {
			res.status(200);
		}
		res.end();
	});
    

});


server.listen(3000);
