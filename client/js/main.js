window.mainLoad = function(){
	window.ItemsVue = new Vue({
		el: '#subscribers',
		data: {
			Subs: [],
			modal_postcode: '',
			new_email: ''
		},
		created: function () {
			this.fetchSubs();
			$('#subscribers').show();
		},
		methods: {
			fetchSubs: function(){
				var self = this;
				$.ajax({
					url: '/subscribers',
					method: 'GET',
					success: function (data) {
						self.Subs = data;
					},
					error: function (error) {
						alert('Error loading. Details:' + JSON.stringify(error));
					}
				});
			},
			submitForm: function (e) {
				var self = this;
				e.preventDefault();
				name = $('#name').val();
				email = $('#email').val();
				postcode = $('#postcode').val();
				params = $.param({ 	name: name, email: email, postcode: postcode }); 
				$.post( '/subscribers?'+params ).done(
					function(data) {
						console.log('Success', data);
						self.fetchSubs();
						self.new_email = '';
						$('#new input').val('');
						$('#name').focus();
					}
				);
			},
			showMap: function (e) {
				console.log(e);
				src = 'https://maps.googleapis.com/maps/api/staticmap?center='+e+'+UK&zoom=14&size=400x400&key=AIzaSyCTwv82UGdh1ygFgNCrW8fytG1aSCdvP7w';
				this.modal_postcode = e;
				$('#map').attr('src',src)
				$('#modal').show();
			},
			closeModal: function (e) {
				$('#modal').hide();
			},
			removeEmail: function (e) {
				var self = this;
				$.ajax({
					url: '/subscribers?email='+e,
					method: 'DELETE',
					success: function (data) {
						self.fetchSubs();
					},
					error: function (error) {
						alert('Error deleting. Details:' + JSON.stringify(error));
					}
				});				
			}
		},
		computed: {
          isInvalid: function () {
			var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
			return !(pattern.test(this.new_email));
          }
        }
	});
};

setAuthToken = function(){
	$.ajaxSetup({
		headers: { 'session':  localStorage.getItem("session")}
	});
};

window.promptForPassword = function(){
	password = prompt("What is the obvious password? Hint: It begins with 'p' and ends with 'd'.");
	$.post( '/session?password='+password )
	.done(
		function(data) {
			console.log('Success', data);
			localStorage.setItem("session", data['session']);
			setAuthToken();
			window.mainLoad();
		}
	).fail(window.promptForPassword);
};

if (localStorage.getItem("session") !== null){
	setAuthToken();
	window.mainLoad();
} else {
	promptForPassword();
}
