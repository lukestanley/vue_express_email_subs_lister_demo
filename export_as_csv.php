<?php

header("Content-Type: text/csv");
header("Content-Disposition: attachment; filename=file.csv");

$db = new SQLite3('database.sqlite');

$results = $db->query('SELECT * FROM subscribers;');
$out = fopen('php://output', 'w');
while ($row = $results->fetchArray()) {
    fputcsv($out, $row);
}

?>
